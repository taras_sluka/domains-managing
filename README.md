# Domains - managing

The system is designed following the principle of client-server architecture.

## How to run in development environments
### Server Side
* Download project from git repository.
* Create db PostgreSQL
* change application.properies
  * Replace ${JDBC_DATABASE_USERNAME} -> PostgreSQL username
    * example - root
  * Replace ${JDBC_DATABASE_PASSWORD} -> PostgreSQL password
    * example - root
  * Replace ${JDBC_DATABASE_URL} -> URL to your db
  * Replace ${GOOGLE_SAFE_BROWSING_KEY} -> google api key
* run spring boot main class 
* open localhost:8080 in RestClient  to testing
### Client Side

 * npm install
 * npm run start
 * open localhost:4200 in browser to testing
	

