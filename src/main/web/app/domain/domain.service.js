"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var rxjs_1 = require("rxjs");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var URL = "http://localhost:8080/dto/domains";
var DomainService = (function () {
    function DomainService(http) {
        this.http = http;
    }
    DomainService.prototype.getDomainsObservable = function () {
        return this.http.get(URL)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    };
    DomainService.prototype.getDomainsPromise = function () {
        return this.http.get(URL).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    };
    DomainService.prototype.extractData = function (res) {
        var body = res.json();
        return body;
    };
    DomainService.prototype.handleErrorObservable = function (error) {
        console.error(error.message || error);
        return rxjs_1.Observable.throw(error.message || error);
    };
    DomainService.prototype.handleErrorPromise = function (error) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    };
    // getDomains(): Promise<Domain[]> {
    //   return this.http.get(URL)
    //     .toPromise()
    //     .then(response => response.json().data as Domain[])
    //     .catch(this.handleError);
    // }
    // private headers = new Headers({'Content-Type': 'application/json'});
    // private API_URL = 'http://localhost:8080/api/domains';
    // constructor(private http: Http) {
    // }
    // getDomains(): Promise<Domain[]> {
    //   return this.http.get(this.API_URL)
    //     .toPromise()
    //     .then(response => response.json().data as Domain[])
    //     .catch(this.handleError);
    // }
    // public createDomain(Domain: Domain): Observable<Domain> {
    //     return this.http
    //         .post(API_URL + '/Domains', Domain)
    //         .map(response => {
    //             return new Domain(response.json());
    //         })
    //         .catch(this.handleError);
    // }
    //
    // public getDomainById(DomainId: number): Observable<Domain> {
    //     return this.http
    //         .get(API_URL + '/Domains/' + DomainId)
    //         .map(response => {
    //             return new Domain(response.json());
    //         })
    //         .catch(this.handleError);
    // }
    //
    // public updateDomain(Domain: Domain): Observable<Domain> {
    //     return this.http
    //         .put(API_URL + '/Domains/' + Domain.id, Domain)
    //         .map(response => {
    //             return new Domain(response.json());
    //         })
    //         .catch(this.handleError);
    // }
    //
    // public deleteDomainById(DomainId: number): Observable<null> {
    //     return this.http
    //         .delete(API_URL + '/Domains/' + DomainId)
    //         .map(response => null)
    //         .catch(this.handleError);
    // }
    //
    DomainService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return DomainService;
}());
DomainService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DomainService);
exports.DomainService = DomainService;
//# sourceMappingURL=domain.service.js.map