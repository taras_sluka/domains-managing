import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DomainListComponent} from './list/domain-list.component';
import {DomainAddComponent} from './add/domain-add.component';
import {DomainEditComponent} from './edit/domain-edit.component';


const heroesRoutes: Routes = [
  {path: 'domains', component: DomainListComponent},
  {path: 'domains/add', component: DomainAddComponent},
  {path: 'domains/:id/edit', component: DomainEditComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(heroesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DomainRoutingModule {
}

