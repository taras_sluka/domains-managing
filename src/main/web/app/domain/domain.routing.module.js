"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var domain_list_component_1 = require("./list/domain-list.component");
var domain_detail_component_1 = require("./detail/domain-detail.component");
var domain_add_component_1 = require("./add/domain-add.component");
var domain_edit_component_1 = require("./edit/domain-edit.component");
var heroesRoutes = [
    { path: 'domains', component: domain_list_component_1.DomainListComponent },
    { path: 'domains/add', component: domain_add_component_1.DomainAddComponent },
    { path: 'domains/:id', component: domain_detail_component_1.DomainDetailComponent },
    { path: 'domains/:id/edit', component: domain_edit_component_1.DomainEditComponent }
];
var DomainRoutingModule = (function () {
    function DomainRoutingModule() {
    }
    return DomainRoutingModule;
}());
DomainRoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forChild(heroesRoutes)
        ],
        exports: [
            router_1.RouterModule
        ]
    })
], DomainRoutingModule);
exports.DomainRoutingModule = DomainRoutingModule;
//# sourceMappingURL=domain.routing.module.js.map