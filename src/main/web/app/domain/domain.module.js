"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var domain_service_1 = require("./domain.service");
var domain_list_component_1 = require("./list/domain-list.component");
var domain_edit_component_1 = require("./edit/domain-edit.component");
var domain_add_component_1 = require("./add/domain-add.component");
var domain_list_item_component_1 = require("./item/domain-list-item.component");
var domain_detail_component_1 = require("./detail/domain-detail.component");
var domain_routing_module_1 = require("./domain.routing.module");
var material_1 = require("@angular/material");
require("hammerjs");
var table_1 = require("@angular/cdk/table");
var DomainModule = (function () {
    function DomainModule() {
    }
    return DomainModule;
}());
DomainModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            material_1.MdButtonModule,
            material_1.MdMenuModule,
            material_1.MdCardModule,
            material_1.MdToolbarModule,
            material_1.MdIconModule,
            table_1.CdkTableModule,
            material_1.MdTableModule,
            domain_routing_module_1.DomainRoutingModule
        ],
        declarations: [
            domain_list_component_1.DomainListComponent,
            domain_edit_component_1.DomainEditComponent,
            domain_add_component_1.DomainAddComponent,
            domain_list_item_component_1.DomainListItemComponent,
            domain_detail_component_1.DomainDetailComponent,
        ],
        providers: [domain_service_1.DomainService]
    })
], DomainModule);
exports.DomainModule = DomainModule;
//# sourceMappingURL=domain.module.js.map