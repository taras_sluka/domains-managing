import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DomainService} from './domain.service';
import {DomainListComponent} from './list/domain-list.component';
import {DomainEditComponent} from './edit/domain-edit.component';
import {DomainAddComponent} from './add/domain-add.component';
import {DomainRoutingModule} from './domain.routing.module';
import {
  MdButtonModule,
  MdCardModule,
  MdIconModule,
  MdMenuModule,
  MdTableModule,
  MdToolbarModule
} from '@angular/material';

import {CdkTableModule} from '@angular/cdk/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MdButtonModule,
    MdMenuModule,
    MdCardModule,
    MdToolbarModule,
    MdIconModule,
    CdkTableModule,
    MdTableModule,
    DomainRoutingModule

  ],
  declarations: [
    DomainListComponent,
    DomainEditComponent,
    DomainAddComponent,
  ],
  providers: [DomainService],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DomainModule {
}

