import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DomainService} from '../domain.service';
import {Router} from '@angular/router';

@Component({
  template: `
    <form novalidate [formGroup]="saveDomainForm" class="add-form">
      <div class="form-group"
           [ngClass]="{
        'has-danger': value.invalid && (value.dirty || value.touched),
        'has-success': value.valid && (value.dirty || value.touched)
   }">
        <label>domain</label>
        <input type="text"
               class="form-control"
               formControlName="value"
               required>
        <div class="add-form-control-feedback"
             *ngIf="value.errors && (value.dirty || value.touched)">
          <span *ngIf="value.errors.required">Domain is required</span>
        </div>
        <div class="add-form-control-feedback">
          <span *ngIf="errorMessage">{{errorMessage}}</span>
        </div>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary" (click)="safeDomain()" [disabled]="!saveDomainForm.valid">Save
        </button>
      </div>
    </form>
  `,
  styleUrls: ['./domain-add.component.css']
})

export class DomainAddComponent implements OnInit {
  saveDomainForm: FormGroup;
  value: FormControl;
  errorMessage: string;

  constructor(private service: DomainService,
              private router: Router) {
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.value = new FormControl('', [
      Validators.pattern('^((?!-)[A-Za-z0-9-]{1,63}(?!-)\\.)+[A-Za-z]{2,6}$'),
      Validators.required
    ]);
  }

  createForm() {
    this.saveDomainForm = new FormGroup({
      value: this.value
    });
  }

  safeDomain() {
    this.service.createDomain(this.saveDomainForm.value).then(
      success => {
        this.router.navigate(['/domains']);
      },
      error => {
        this.errorMessage = error.json()[0].message;
        console.log('errorMessage', this.errorMessage);
      }
    );
  }
}
