"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var domain_service_1 = require("../domain.service");
var router_1 = require("@angular/router");
require("rxjs/add/observable/of");
var DomainListComponent = (function () {
    function DomainListComponent(service, route) {
        this.service = service;
        this.route = route;
        this.domains = [];
        this.displayedColumns = ['id', 'value'];
        this.dataSource = new ExampleDataSource();
    }
    DomainListComponent.prototype.ngOnInit = function () {
        console.log('DomainListComponent');
        this.observableDomains = this.service.getDomainsObservable();
        // this.domains = [
        //   {'id': 1, 'value': 'value'},
        //   {'id': 1, 'value': 'value'},
        //   {'id': 1, 'value': 'value'},
        //   {'id': 1, 'value': 'value'},
        // ];
        // this.observableDomains.subscribe(
        //   domains => this.domains = domains,
        //   error => this.errorMessage = <any>error
    };
    return DomainListComponent;
}());
DomainListComponent = __decorate([
    core_1.Component({
        selector: 'domain-list',
        template: "\n    <div class=\"example-container mat-elevation-z8\">\n      <table>\n        <tr>\n          <td style=\"width: 10%\">id</td>\n          <td style=\"width: 60%\">value</td>\n          <td style=\"width: 10%\">malware</td>\n          <td style=\"width: 10%\"></td>\n          <td style=\"width: 10%\"></td>\n        </tr>\n        <tr *ngFor=\"let domain of domains\">\n          <td>{{domain.id}}</td>\n          <td>{{domain.value}}</td>\n          <td>malware</td>\n          <td>\n            <button md-button value=\"delete\"></button>\n          </td>\n          <td>\n            <button md-button value=\"edit\"></button>\n          </td>\n        </tr>\n      </table>\n    </div>\n  ",
        styleUrls: ['./domain-list.component.css']
    }),
    __metadata("design:paramtypes", [domain_service_1.DomainService,
        router_1.ActivatedRoute])
], DomainListComponent);
exports.DomainListComponent = DomainListComponent;
//# sourceMappingURL=domain-list.component.js.map