import {Component, OnInit} from '@angular/core';
import {DomainService} from '../domain.service';
import {Domain} from '../domain';
import 'rxjs/add/observable/of';

@Component({
  template: `
    <div class="list-container mat-elevation-z8">
      <table>
        <tr>
          <td style="width: 10%">id</td>
          <td style="width: 60%">value</td>
          <td style="width: 10%">malware</td>
          <td style="width: 10%"></td>
          <td style="width: 10%"></td>
        </tr>
        <tr *ngFor="let domain of domains">
          <td style="text-align: center">{{domain.id}}</td>
          <td>{{domain.value}}</td>
          <td>{{domain.domainState}}</td>
          <td>
            <button md-button (click)="removeDomainById(domain.id)">delete</button>
          </td>
          <td>
            <button md-button>edit</button>
          </td>
        </tr>
      </table>
      <div *ngIf="errorMessage"> {{errorMessage}}</div>
    </div>
  `,
  styleUrls: ['./domain-list.component.css']
})
export class DomainListComponent implements OnInit {
  domains: Domain[];
  errorMessage: string;

  constructor(private service: DomainService) {
  }

  ngOnInit() {
    this.service.getDomains().then(
      domains => this.domains = domains,
      error => this.errorMessage = <any>error
    );
  }

  removeDomainById(id: number) {
    console.log('delete id = ' + id);
    this.service.deleteDomainById(id).then(
      success => {
        this.domains = this.domains.filter(domain => domain.id !== id);
      },
      error => this.errorMessage = <any>error
    );
  }
}

