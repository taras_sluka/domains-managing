import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Domain} from './domain';

const URL = '/api/domains';

@Injectable()
export class DomainService {

  constructor(private http: Http) {
  }

  getDomains(): Promise<Domain[]> {
    return this.http.get(URL).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  deleteDomainById(id: number): Promise<null> {
    return this.http.delete(URL + '/' + id).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  public createDomain(Domain: Domain): Promise<Domain> {
    return this.http
      .post(URL, Domain).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }


  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  public updateDomain(Domain: Domain): Promise<Domain> {
    return this.http
      .put(URL + '/id/' + Domain.id, Domain).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
