import {Component, OnInit} from '@angular/core';

@Component({
  template: `
    <p>
      page-not-found works!
    </p>
  `,
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {


  ngOnInit() {
    console.log('PageNotFoundComponent');
  }
}
