package com.sluka.taras.domainsmanaging.exception;

import org.springframework.context.support.MessageSourceAccessor;

import java.util.Locale;

public abstract class DomainsManagingException extends RuntimeException {

    private MessageSourceAccessor messageAcceptor;

    private String messageCode;

    private Object[] args;

    public DomainsManagingException(String messageCode, Object[] args, MessageSourceAccessor messageSourceAccessor) {
        this.messageAcceptor = messageSourceAccessor;
        this.messageCode = messageCode;
        this.args = args;
    }
    @Override
    public String getMessage() {
        return messageAcceptor
                .getMessage(messageCode, args, Locale.ENGLISH);
    }

    @Override
    public String getLocalizedMessage() {
        return messageAcceptor
                .getMessage(messageCode, args);
    }
}
