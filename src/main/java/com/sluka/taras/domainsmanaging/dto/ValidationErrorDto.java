package com.sluka.taras.domainsmanaging.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
@EqualsAndHashCode(callSuper = true)
@Data
public class ValidationErrorDto extends ErrorMessageDto {

    private String field;

    public ValidationErrorDto(String field, String message) {
        super(message);
        this.field = field;
    }
}

