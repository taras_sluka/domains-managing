package com.sluka.taras.domainsmanaging.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import java.util.Locale;

@Configuration
public class LocalisationConfig {
    private MessageSource messageSource;

    @Autowired
    public LocalisationConfig(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Bean(name = "localeResolver")
    public LocaleResolver getLocaleResolver() {
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setDefaultLocale(Locale.ENGLISH);
        return localeResolver;
    }

    @Bean(name = "messageSourceAccessor")
    public MessageSourceAccessor getMessageSourceAccessor() {
        return new MessageSourceAccessor(messageSource, Locale.ENGLISH);

    }
}