package com.sluka.taras.domainsmanaging.config;

import com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.GoogleSafeBrowsingResponseErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;

@Configuration
public class SpringConfig extends WebMvcConfigurerAdapter {

  private MessageSource messageSource;
  private GoogleSafeBrowsingResponseErrorHandler googleSafeBrowsingResponseErrorHandler;


  private final static String[] UI_FILES_MAPPING = {
    "*.*.js", "*.*.js.map", "*.*.woff2", "*.*.svg",
    "*.*.ttf", "*.*.eot", "*.*.woff"
  };
  private final static String UI_FILES_LOCATION = "classpath:/static/";

  private final static String[] UI_ENDPOINTS = {"/", "/**"};
  private final static String UI_ENDPOINTS_LOCATION = "classpath:/static/index.html";

  @Autowired
  public SpringConfig(MessageSource messageSource,
                      GoogleSafeBrowsingResponseErrorHandler googleSafeBrowsingResponseErrorHandler) {
    this.messageSource = messageSource;
    this.googleSafeBrowsingResponseErrorHandler = googleSafeBrowsingResponseErrorHandler;

  }

  @Bean(name = "googleRestTemplate")
  public RestTemplate googleRestTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setErrorHandler(googleSafeBrowsingResponseErrorHandler);
    return restTemplate;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(UI_FILES_MAPPING)
      .addResourceLocations(UI_FILES_LOCATION);
    registry.addResourceHandler(UI_ENDPOINTS)
      .addResourceLocations(UI_ENDPOINTS_LOCATION)
      .resourceChain(true)
      .addResolver(new PathResourceResolver() {
        @Override
        protected Resource getResource(String resourcePath, Resource location) throws IOException {
          return location.exists() && location.isReadable() ? location : null;
        }
      });
  }


  @Override
  public Validator getValidator() {
    LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
    factory.setValidationMessageSource(messageSource);
    return factory;
  }


}
