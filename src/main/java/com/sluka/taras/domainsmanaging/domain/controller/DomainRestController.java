package com.sluka.taras.domainsmanaging.domain.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.sluka.taras.domainsmanaging.domain.model.Domain;
import com.sluka.taras.domainsmanaging.domain.service.DomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/domains")
public class DomainRestController {

    private DomainService domainService;
    private MessageSourceAccessor messageSourceAccessor;

    @Autowired
    public DomainRestController(DomainService domainService,
                                MessageSourceAccessor messageSourceAccessor) {
        this.domainService = domainService;
        this.messageSourceAccessor = messageSourceAccessor;
    }

    @GetMapping
    public List<Domain> getAll() {
        log.debug("get all domains");
        return domainService.getAll();
    }

    @GetMapping("/{id}")
    public Domain getById(@PathVariable Long id) {
        return domainService.getById(id);
    }

    @PostMapping
    public Domain save(@RequestBody @Validated @JsonView(Domain.Save.class) Domain domain) {
        return domainService.save(domain);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {

        domainService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Domain updateById(@PathVariable Long id, @RequestBody @Validated Domain domain) {
        if (!id.equals(domain.getId())) {
            throw new DomainConflictException(messageSourceAccessor);
        }
        return domainService.update(id, domain);
    }
}
