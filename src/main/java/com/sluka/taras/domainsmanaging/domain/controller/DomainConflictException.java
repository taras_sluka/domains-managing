package com.sluka.taras.domainsmanaging.domain.controller;

import com.sluka.taras.domainsmanaging.exception.DomainsManagingException;
import org.springframework.context.support.MessageSourceAccessor;

public class DomainConflictException extends DomainsManagingException {

    private static final String DOMAIN_CONFLICT_EXCEPTION = "domain.conflict.exception";

    public DomainConflictException(MessageSourceAccessor messageSourceAccessor) {
        super(DOMAIN_CONFLICT_EXCEPTION, new Object[]{}, messageSourceAccessor);
    }
}