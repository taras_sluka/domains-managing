package com.sluka.taras.domainsmanaging.domain.model.validation;


import com.sluka.taras.domainsmanaging.domain.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueDomainValueValidator implements ConstraintValidator<UniqueDomainValue, String> {

    private final DomainService domainService;

    @Autowired
    public UniqueDomainValueValidator(DomainService domainService) {
        this.domainService = domainService;
    }

    @Override
    public void initialize(UniqueDomainValue constraintAnnotation) {
    }
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !domainService.existsByValue(value);
    }

}
