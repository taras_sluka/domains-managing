package com.sluka.taras.domainsmanaging.domain.service.exception;

import com.sluka.taras.domainsmanaging.exception.DomainsManagingException;
import org.springframework.context.support.MessageSourceAccessor;

public class DomainNotFoundException extends DomainsManagingException {

    private static final String DOMAIN_NOT_FOUND_CODE = "domain.not.found";

    public DomainNotFoundException(String fieldName, String fieldValue, MessageSourceAccessor messageSourceAccessor) {
        super(DOMAIN_NOT_FOUND_CODE, new Object[]{fieldName, fieldValue}, messageSourceAccessor);
    }

    public DomainNotFoundException(String fieldName, Long fieldValue, MessageSourceAccessor messageSourceAccessor) {
        super(DOMAIN_NOT_FOUND_CODE, new Object[]{fieldName, fieldValue}, messageSourceAccessor);
    }
}
