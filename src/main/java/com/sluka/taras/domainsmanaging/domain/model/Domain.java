package com.sluka.taras.domainsmanaging.domain.model;


import com.fasterxml.jackson.annotation.JsonView;
import com.sluka.taras.domainsmanaging.domain.model.validation.UniqueDomainValue;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Domain extends BaseEntity<Long> {

  @JsonView(Save.class)
  @NotNull(message = "{validation.domain.null}")
  @Pattern(regexp = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$", message = "{validation.domain.not.valid}")
  @UniqueDomainValue(message = "{validation.domain.exist}")
  private String value;

  @Transient
  private DomainState domainState = DomainState.undefined;

  public enum DomainState {
    undefined, malware, safe
  }

  public interface Save {
  }
}
