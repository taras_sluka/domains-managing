package com.sluka.taras.domainsmanaging.domain.service;

import com.sluka.taras.domainsmanaging.domain.model.Domain;

import java.util.List;


public interface DomainService {

    List<Domain> getAll();

    Domain getById(Long id);

    Domain save(Domain domain);

    Domain update(Long id, Domain domain);

    void deleteById(Long id);

    Boolean existsByValue(String domainValue);

    Domain getByValue(String domainValue);

}
