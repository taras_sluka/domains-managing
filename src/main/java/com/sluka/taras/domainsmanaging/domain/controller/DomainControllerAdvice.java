package com.sluka.taras.domainsmanaging.domain.controller;

import com.sluka.taras.domainsmanaging.dto.ErrorMessageDto;
import com.sluka.taras.domainsmanaging.dto.ValidationErrorDto;
import com.sluka.taras.domainsmanaging.domain.service.exception.DomainNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;


@Slf4j
@RestControllerAdvice
public class DomainControllerAdvice {

    private ValidationErrorMapper validationErrorMapper;

    @Autowired
    public DomainControllerAdvice(ValidationErrorMapper validationErrorMapper) {
        this.validationErrorMapper = validationErrorMapper;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<ValidationErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return validationErrorMapper.mapErrorsToAPIes(e);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DomainNotFoundException.class)
    public ErrorMessageDto domainNotFoundException(DomainNotFoundException e) {
        return new ErrorMessageDto(e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DomainConflictException.class)
    public ErrorMessageDto domainNotFoundException(DomainConflictException e) {
        return new ErrorMessageDto(e.getMessage());
    }
}
