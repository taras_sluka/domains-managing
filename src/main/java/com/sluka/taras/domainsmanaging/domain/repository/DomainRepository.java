package com.sluka.taras.domainsmanaging.domain.repository;

import com.sluka.taras.domainsmanaging.domain.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DomainRepository extends JpaRepository<Domain, Long> {
    Domain getByValue(String value);

    Boolean existsByValue(String value);
}
