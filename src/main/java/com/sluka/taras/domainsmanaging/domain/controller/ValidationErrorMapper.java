package com.sluka.taras.domainsmanaging.domain.controller;

import com.sluka.taras.domainsmanaging.dto.ValidationErrorDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;

@Component
public class ValidationErrorMapper {

    public List<ValidationErrorDto> mapErrorsToAPIes(MethodArgumentNotValidException errors) {
        List<ObjectError> objectErrors = errors.getBindingResult().getAllErrors();
        return transformErrorObjectsToErrorAPIes(objectErrors);
    }

    private List<ValidationErrorDto> transformErrorObjectsToErrorAPIes(List<ObjectError> errorList) {
        List<ValidationErrorDto> validationErrorApis = new ArrayList<>();
        errorList.forEach(
                error -> validationErrorApis
                        .add(transformErrorObjectToAPIError(error)));
        return validationErrorApis;
    }

    private ValidationErrorDto transformErrorObjectToAPIError(ObjectError objectError) {
        return new ValidationErrorDto(((FieldError) objectError).getField(), objectError.getDefaultMessage());


    }
}
