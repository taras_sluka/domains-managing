package com.sluka.taras.domainsmanaging.domain.service.impl;


import com.sluka.taras.domainsmanaging.domain.model.Domain;
import com.sluka.taras.domainsmanaging.domain.repository.DomainRepository;
import com.sluka.taras.domainsmanaging.domain.service.DomainService;
import com.sluka.taras.domainsmanaging.domain.service.exception.DomainNotFoundException;
import com.sluka.taras.domainsmanaging.safeBrowsing.exception.SafeBrowsingException;
import com.sluka.taras.domainsmanaging.safeBrowsing.service.SafeBrowsingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service(value = "domainService")
public class DomainServiceImpl implements DomainService {

    private DomainRepository domainRepository;
    private MessageSourceAccessor messageSourceAccessor;
    private SafeBrowsingService safeBrowsingService;

    @Autowired
    public DomainServiceImpl(DomainRepository domainRepository,
                             MessageSourceAccessor messageSourceAccessor,
                             SafeBrowsingService safeBrowsingService) {
        this.domainRepository = domainRepository;
        this.messageSourceAccessor = messageSourceAccessor;
        this.safeBrowsingService = safeBrowsingService;
    }

    @Override
    public Domain save(Domain domain) {
        return Optional
                .ofNullable(domainRepository.save(domain))
                .map(this::addIsMalwareProperty)
                .get();
    }

    @Override
    public Domain update(Long id, Domain domain) {
        if (domainRepository.exists(id)) {
            throw new DomainNotFoundException("id", id, messageSourceAccessor);
        }
        return save(domain);

    }
    @Override
    public void deleteById(Long id) {
        Optional.of(domainRepository)
                .filter(repo -> repo.exists(id))
                .orElseThrow(() -> new DomainNotFoundException("id", id, messageSourceAccessor))
                .delete(id);
    }

    @Override
    public Domain getById(Long id) {
        return Optional
                .ofNullable(domainRepository.findOne(id))
                .map(this::addIsMalwareProperty)
                .orElseThrow(() -> new DomainNotFoundException("id", id, messageSourceAccessor));
    }


    @Override
    public List<Domain> getAll() {
        List<Domain> domains = domainRepository.findAll();
        return addIsMalwareProperty(domains);
    }

    private List<Domain> addIsMalwareProperty(List<Domain> domains) {
        List<String> stringDomains = getDomainValues(domains);
        try {
            Map<String, Boolean> safeBrowsingDomains = safeBrowsingService.isMalware(stringDomains);
            domains.forEach(domain -> {
                boolean isMalware = safeBrowsingDomains.get(domain.getValue());
                Domain.DomainState domainState = isMalwareTransform(isMalware);
                domain.setDomainState(domainState);
            });
        } catch (SafeBrowsingException e) {
            log.error(e.getMessage());
        }
        return domains;
    }
    private List<String> getDomainValues(List<Domain> domains) {
        return domains.stream()
                .map(Domain::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public Domain getByValue(String domainValue) {
        return Optional
                .ofNullable(domainRepository.getByValue(domainValue))
                .map(this::addIsMalwareProperty)
                .orElseThrow(() -> new DomainNotFoundException("value", domainValue, messageSourceAccessor));
    }

    @Override
    public Boolean existsByValue(String domainValue) {
        return domainRepository.existsByValue(domainValue);
    }

    private Domain addIsMalwareProperty(Domain domain) {
        try {
            boolean isMalware = safeBrowsingService.isMalware(domain.getValue());
            Domain.DomainState domainState = isMalwareTransform(isMalware);
            domain.setDomainState(domainState);
        } catch (SafeBrowsingException e) {
            log.error(e.getMessage());
        }
        return domain;
    }

    private Domain.DomainState isMalwareTransform(Boolean isMalware) {
        return isMalware ? Domain.DomainState.malware : Domain.DomainState.safe;
    }
}
