package com.sluka.taras.domainsmanaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomainsManagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomainsManagingApplication.class, args);
	}
}
