package com.sluka.taras.domainsmanaging.safeBrowsing.service;

import java.util.List;
import java.util.Map;

public interface SafeBrowsingService {

    Boolean isMalware(String domain);

    Map<String, Boolean> isMalware(List<String> domains);

}
