package com.sluka.taras.domainsmanaging.safeBrowsing.exception;

import com.sluka.taras.domainsmanaging.exception.DomainsManagingException;
import org.springframework.context.support.MessageSourceAccessor;

public abstract class SafeBrowsingException extends DomainsManagingException {

    public SafeBrowsingException(String messageCode, Object[] args, MessageSourceAccessor messageSourceAccessor) {
        super(messageCode, args, messageSourceAccessor);
    }
}
