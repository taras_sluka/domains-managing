package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception;

import com.sluka.taras.domainsmanaging.safeBrowsing.exception.SafeBrowsingException;
import org.springframework.context.support.MessageSourceAccessor;

public class SafeBrowsingServiceUnavailable extends SafeBrowsingException {

    private static final String SAFE_BROWSING_SERVICE_UNAVAILABLE = "safe.browsing.service.unavailable";

    public SafeBrowsingServiceUnavailable(MessageSourceAccessor messageSourceAccessor) {
        super(SAFE_BROWSING_SERVICE_UNAVAILABLE, new Object[]{}, messageSourceAccessor);
    }
}
