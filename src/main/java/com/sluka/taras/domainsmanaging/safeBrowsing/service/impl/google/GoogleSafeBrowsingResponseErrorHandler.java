package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google;

import com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception.SafeBrowsingBadRequestException;
import com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception.SafeBrowsingNotAuthorized;
import com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception.SafeBrowsingServiceUnavailable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;


@Component
public class GoogleSafeBrowsingResponseErrorHandler implements ResponseErrorHandler {

    private MessageSourceAccessor messageSourceAccessor;

    @Autowired
    public GoogleSafeBrowsingResponseErrorHandler(MessageSourceAccessor messageSourceAccessor) {
        this.messageSourceAccessor = messageSourceAccessor;
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        int statusCode = response.getStatusCode().value();
        return !(statusCode >= 200 && statusCode < 300);
    }
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        HttpStatus httpStatusCode = response.getStatusCode();
        switch (httpStatusCode) {
            case FORBIDDEN:
                throw new SafeBrowsingNotAuthorized(messageSourceAccessor);
            case UNAUTHORIZED:
                throw new SafeBrowsingNotAuthorized(messageSourceAccessor);
            case SERVICE_UNAVAILABLE:
                throw new SafeBrowsingServiceUnavailable(messageSourceAccessor);
            default:
                throw new SafeBrowsingBadRequestException(messageSourceAccessor);
        }
    }
}
