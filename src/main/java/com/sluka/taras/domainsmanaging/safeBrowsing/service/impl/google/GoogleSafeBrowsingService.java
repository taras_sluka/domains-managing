package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google;

import com.sluka.taras.domainsmanaging.safeBrowsing.service.SafeBrowsingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GoogleSafeBrowsingService implements SafeBrowsingService {

    @Value(value = "${google.safe.browsing.url}")
    private String urlAddress;

    @Value(value = "${google.safe.browsing.body.delimiter}")
    private String googleRequestBodyDelimiter;

    @Value(value = "${google.safe.browsing.response.malware}")
    private String malware;

    @Value(value = "${google.safe.browsing.max.domains}")
    private int maxAllowCheckDomains;

    private GoogleRequestParametersBuilder googleRequestParametersBuilder;

    private RestTemplate restTemplate;

    @Autowired
    public GoogleSafeBrowsingService(
            @Qualifier("googleRestTemplate") RestTemplate googleRestTemplate,
            GoogleRequestParametersBuilder googleRequestParametersBuilder) {
        this.googleRequestParametersBuilder = googleRequestParametersBuilder;
        this.restTemplate = googleRestTemplate;
    }

    @Override
    public Boolean isMalware(String domain) {
        URI uri = getURIForGetMethod(domain);
        ResponseEntity<String> resultResponseEntity = restTemplate.getForEntity(uri, String.class);
        return resultResponseEntity.getStatusCode() == HttpStatus.OK;
    }

    private URI getURIForGetMethod(String domain) {
        Map<String, String> params = googleRequestParametersBuilder.getParamsForGetReques(domain);
        return prepareURIWithParams(params);
    }

    @Override
    public Map<String, Boolean> isMalware(List<String> domains) {
        Map<String, Boolean> resultCheck;
        if (domains.size() >= maxAllowCheckDomains) {
            resultCheck = checkIsMalwareDomainsMoreThanGoogleAllowed(domains);
        } else {
            resultCheck = checkIsMalwareDomains(domains);
        }
        return resultCheck;
    }

    private Map<String, Boolean> checkIsMalwareDomainsMoreThanGoogleAllowed(List<String> domains) {
        Map<String, Boolean> resultCheck = new HashMap<>();

        int indexEnd = maxAllowCheckDomains;
        for (int indexStart = 0; indexStart != indexEnd; indexStart += maxAllowCheckDomains) {
            Map<String, Boolean> subResultCkeck = checkIsMalwareDomains(domains.subList(indexStart, indexEnd));
            resultCheck.putAll(subResultCkeck);
            indexEnd += maxAllowCheckDomains;
            if (indexEnd > domains.size()) {
                indexEnd = domains.size();
            }
        }
        return resultCheck;
    }

    private Map<String, Boolean> checkIsMalwareDomains(List<String> domains) {
        URI uri = getURIForPOSTMethod();

        String requestBody = formationRequestBody(domains);
        String result = restTemplate.postForObject(uri, requestBody, String.class);
        return parseGooglePostResult(domains, result);
    }

    private URI getURIForPOSTMethod() {
        Map<String, String> params = googleRequestParametersBuilder.getParamsForPostRequest();
        return prepareURIWithParams(params);
    }

    private String formationRequestBody(List<String> domains) {
        String size = String.valueOf(domains.size()).concat(googleRequestBodyDelimiter);
        String domainsInString =
                domains
                        .stream()
                        .collect(Collectors.joining(googleRequestBodyDelimiter));
        return size.concat(domainsInString).concat(googleRequestBodyDelimiter);
    }

    private Map<String, Boolean> parseGooglePostResult(List<String> domains, String googleResult) {
        Map<String, Boolean> domainsMapperResult = new HashMap<>();
        String[] resultArr = googleResult.split(googleRequestBodyDelimiter);

        for (int i = 0; i < resultArr.length; i++) {
            boolean isMalware = isMalwareDomain(resultArr[i]);
            domainsMapperResult.put(domains.get(i), isMalware);
        }
        return domainsMapperResult;
    }

    private boolean isMalwareDomain(String googleRequestResult) {
        return googleRequestResult.compareTo(malware) == 0;
    }


    private URI prepareURIWithParams(Map<String, String> params) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlAddress);
        if (params != null) {
            params.entrySet()
                    .stream()
                    .filter(e -> !StringUtils.isEmpty(e.getValue()))
                    .forEach(e -> builder.queryParam(e.getKey(), e.getValue()));
        }
        return builder.build().encode().toUri();
    }
}
