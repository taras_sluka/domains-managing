package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception;

import com.sluka.taras.domainsmanaging.safeBrowsing.exception.SafeBrowsingException;
import org.springframework.context.support.MessageSourceAccessor;

public class SafeBrowsingBadRequestException extends SafeBrowsingException {

    private static final String SAFE_BROWSING_BAD_REQUEST = "safe.browsing.bad.bequest";

    public SafeBrowsingBadRequestException(MessageSourceAccessor messageSourceAccessor) {
        super(SAFE_BROWSING_BAD_REQUEST, new Object[]{}, messageSourceAccessor);
    }
}
