package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class GoogleRequestParametersBuilder {

    @Value(value = "${google.safe.browsing.key}")
    private String key;

    @Value(value = "${google.safe.browsing.pver}")
    private String pver;

    @Value(value = "${google.safe.browsing.appver}")
    private String appver;

    @Value(value = "${google.safe.browsing.client}")
    private String client;

    private Map<String, String> params;

    @PostConstruct
    private void init() throws Exception {
        params = new HashMap<String, String>() {{
            put("key", key);
            put("pver", pver);
            put("appver", appver);
            put("client", client);
        }};
        params = Collections.unmodifiableMap(params);
    }

    Map<String, String> getParamsForGetReques(String domain) {
        Map<String, String> params = new HashMap<>(getParamsForPostRequest());
        params.put("url", domain);
        return Collections.unmodifiableMap(params);
    }

     Map<String, String> getParamsForPostRequest() {
        return params;
    }
}
