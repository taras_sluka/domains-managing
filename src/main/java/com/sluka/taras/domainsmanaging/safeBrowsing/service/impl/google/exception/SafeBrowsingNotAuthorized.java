package com.sluka.taras.domainsmanaging.safeBrowsing.service.impl.google.exception;

import com.sluka.taras.domainsmanaging.safeBrowsing.exception.SafeBrowsingException;
import org.springframework.context.support.MessageSourceAccessor;

public class SafeBrowsingNotAuthorized extends SafeBrowsingException {

    private static final String SAFE_BROWSING_SERVICE_UNAVAILABLE = "safe.browsing.not.authorized";

    public SafeBrowsingNotAuthorized(MessageSourceAccessor messageSourceAccessor) {
        super(SAFE_BROWSING_SERVICE_UNAVAILABLE, new Object[]{}, messageSourceAccessor);
    }
}
