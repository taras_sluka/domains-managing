CREATE TABLE domain
(
  id    BIGINT AUTO_INCREMENT NOT NULL,
  value VARCHAR(255)          NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (id, value)
);