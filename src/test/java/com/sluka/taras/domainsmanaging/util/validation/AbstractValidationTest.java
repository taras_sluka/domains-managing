package com.sluka.taras.domainsmanaging.util.validation;

import org.junit.BeforeClass;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public abstract class AbstractValidationTest {

    protected static Validator validator;

    @BeforeClass
    public static void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }


    protected static <T> void testField_Successfully(T entity) {

        Set<ConstraintViolation<T>> constraintViolations =
                validator.validate(entity);
        assertEquals(0, constraintViolations.size());
    }

    protected static <T> void testField_Failure(T entity, String expectedErrorMessage) {
        Set<ConstraintViolation<T>> constraintViolations =
                validator.validate(entity);
        assertEquals(1, constraintViolations.size());
        assertEquals(
                expectedErrorMessage,
                constraintViolations.iterator().next().getMessage()
        );
    }
}