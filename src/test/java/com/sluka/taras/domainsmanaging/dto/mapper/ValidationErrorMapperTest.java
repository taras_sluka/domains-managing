package com.sluka.taras.domainsmanaging.dto.mapper;

import com.sluka.taras.domainsmanaging.dto.ValidationErrorDto;
import com.sluka.taras.domainsmanaging.domain.controller.ValidationErrorMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mock;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Parameterized.class)
public class ValidationErrorMapperTest {

    private static final String OBJECT_NAME = "Value";
    private static final String VALUE_FIELD = "value";
    private static final String VALUE_MESSAGE_1 = "Domain already exist";
    private static final String VALUE_MESSAGE_2 = "Domain is not valid";

    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;

    @Mock
    private BindingResult bindingResult;

    private ValidationErrorMapper validationErrorMapper;

    @Before
    public void setUp() {
        initMocks(this);
        validationErrorMapper = new ValidationErrorMapper();
    }

    @Parameters
    public static List<Object[]> data() {
        ValidationErrorDto DEFAULT_ERROR_API_1 = new ValidationErrorDto(VALUE_FIELD, VALUE_MESSAGE_1);
        ValidationErrorDto DEFAULT_ERROR_API_2 = new ValidationErrorDto(VALUE_FIELD, VALUE_MESSAGE_2);

        ObjectError DEFAULT_OBJ_ERROR_1 = new FieldError(OBJECT_NAME, VALUE_FIELD, VALUE_MESSAGE_1);
        ObjectError DEFAULT_OBJ_ERROR_2 = new FieldError(OBJECT_NAME, VALUE_FIELD, VALUE_MESSAGE_2);

        return Arrays.asList(new Object[][]{
                {new ArrayList(),
                        new ArrayList()},
                {Collections.singletonList(DEFAULT_OBJ_ERROR_1),
                        Collections.singletonList(DEFAULT_ERROR_API_1)},

                {Arrays.asList(DEFAULT_OBJ_ERROR_1, DEFAULT_OBJ_ERROR_2),
                        Arrays.asList(DEFAULT_ERROR_API_1, DEFAULT_ERROR_API_2)},

                {Arrays.asList(DEFAULT_OBJ_ERROR_1, DEFAULT_OBJ_ERROR_2, DEFAULT_OBJ_ERROR_1),
                        Arrays.asList(DEFAULT_ERROR_API_1, DEFAULT_ERROR_API_2, DEFAULT_ERROR_API_1)}
        });
    }

    @Parameter
    public List<ObjectError> objectErrors;

    @Parameter(1)
    public List<ValidationErrorDto> expectedValidationErrors;


    @Test
    public void mapErrorsToAPIes() throws Exception {
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        when(bindingResult.getAllErrors()).thenReturn(objectErrors);

        List<ValidationErrorDto> actualValidationErrors = validationErrorMapper.mapErrorsToAPIes(methodArgumentNotValidException);

        assertEquals(expectedValidationErrors, actualValidationErrors);
    }
}