package com.sluka.taras.domainsmanaging.domain.service.impl;

import com.sluka.taras.domainsmanaging.domain.model.Domain;
import com.sluka.taras.domainsmanaging.domain.repository.DomainRepository;
import com.sluka.taras.domainsmanaging.domain.service.DomainService;
import com.sluka.taras.domainsmanaging.domain.service.exception.DomainNotFoundException;
import com.sluka.taras.domainsmanaging.safeBrowsing.service.SafeBrowsingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.MessageSourceAccessor;

import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DomainServiceImplTest {

    private static final Long DEFAULT_ID = 1L;

    @Mock
    private DomainRepository domainRepository;

    @Mock
    private MessageSourceAccessor messageSourceAccessor;
    @Mock
    private SafeBrowsingService safeBrowsingService;

    @Mock
    private Domain domainExpected;

    private DomainService domainService;


    @Before
    public void setUp() throws Exception {
        domainService = new DomainServiceImpl(domainRepository, messageSourceAccessor, safeBrowsingService);
    }

    @Test
    public void getById_Successfully() {
        when(domainRepository.findOne(DEFAULT_ID)).thenReturn(domainExpected);

        Domain domainActual = domainService.getById(DEFAULT_ID);

        verifyDomainRepository(repository -> repository.findOne(DEFAULT_ID));

        assertEquals(domainExpected, domainActual);
    }

    @Test(expected = DomainNotFoundException.class)
    public void getById_DomainNotExistById() {
        when(domainRepository.findOne(DEFAULT_ID))
                .thenThrow(new DomainNotFoundException("id", DEFAULT_ID, messageSourceAccessor));

        domainService.getById(DEFAULT_ID);

        verifyDomainRepository(repository -> repository.findOne(DEFAULT_ID));
    }

    private void verifyDomainRepository(Consumer<DomainRepository> repositoryConsumer) {
        repositoryConsumer.accept(verify(domainRepository));
        verifyNoMoreInteractions(domainRepository);
    }

}