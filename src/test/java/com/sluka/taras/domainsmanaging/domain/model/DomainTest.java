package com.sluka.taras.domainsmanaging.domain.model;

import com.sluka.taras.domainsmanaging.util.validation.AbstractValidationTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Enclosed.class)
public class DomainTest {

    private static Domain domain;

    public abstract static class AbstractValidationDomainTest extends AbstractValidationTest {

        @Before
        public void setUp() {
            domain = DefaultDomainFactory.getDefaultDomain();
        }
    }

    @RunWith(Enclosed.class)
    public static class ValueValidationsTest {
        @Ignore
        public static class ValueValidationTest extends AbstractValidationDomainTest {
            @Test
            public void companyName_IsNotNull() {
                domain.setValue(null);
                testField_Failure(domain, "may not be null");
            }
        }

        @Ignore
        @RunWith(Parameterized.class)
        public static class ValueReqExSuccessfullyTest extends AbstractValidationDomainTest {
            @Parameter
            public String validDomain;
            @Parameters(name = "{index}: value: {0}")
            public static Collection validDomains() {
                return Arrays.asList(
                        "www.google.com",
                        "google.com",
                        "google123.com",
                        "google-info.com",
                        "sub.google.com",
                        "sub.google-info.com",
                        "google.com.au",
                        "g.co",
                        "google.t.t.co"
                );
            }

            @Test
            public void value_isValid() {
                domain.setValue(validDomain);
                testField_Successfully(domain);
            }
        }

        @Ignore
        @RunWith(Parameterized.class)
        public static class ValueReqExFailureTest extends AbstractValidationDomainTest {

            @Parameter
            public String invalidDomain;
            @Parameters(name = "{index}: value: {0}")
            public static Collection invalidDomains() {
                return Arrays.asList(
                        "google.t.t.c",
                        "google,com",
                        "google",
                        "google.123",
                        ".com",
                        "google.a",
                        "google.com/users",
                        "-google.com",
                        "google-.com",
                        "sub.-google.com",
                        "sub.google-.com",
                        ""
                );
            }

            @Test
            public void value_isInValid() {
                domain.setValue(invalidDomain);
                testField_Failure(domain, "domain is not valid");
            }
        }
    }
}