package com.sluka.taras.domainsmanaging.domain.model;

public class DefaultDomainFactory {

    private static final Long DEFAULT_ID = 1L;

    private static final String DEFAULT_DOMAIN = "www.google.com";

    public static Domain getDefaultDomain() {
        Domain domain = new Domain();
        domain.setId(DEFAULT_ID);
        domain.setValue(DEFAULT_DOMAIN);
        return domain;
    }
}
